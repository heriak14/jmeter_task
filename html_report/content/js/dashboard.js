/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9297619047619048, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=1"], "isController": false}, {"data": [0.95, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=true"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=itinerary"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/WebTours\/"], "isController": false}, {"data": [0.965, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=flights"], "isController": false}, {"data": [0.935, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/reservations.pl?page=welcome"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.95, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/login.pl?intro=true"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=itinerary"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/WebTours\/header.html"], "isController": false}, {"data": [0.955, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=search"], "isController": false}, {"data": [0.9816666666666667, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/reservations.pl"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/WebTours\/home.html"], "isController": false}, {"data": [0.9675, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?in=home"], "isController": false}, {"data": [0.945, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/login.pl"], "isController": false}, {"data": [0.945, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=home"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/itinerary.pl"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2000, 0, 0.0, 276.0025000000005, 0, 1175, 422.0, 496.9499999999998, 789.94, 35.08710373502219, 57.11522604866582, 15.49791204320977], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=1", 100, 0, 0.0, 302.3600000000001, 132, 470, 381.00000000000006, 423.5499999999999, 469.72999999999985, 2.0981515285033887, 2.1005078510207507, 0.8298353213319065], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=true", 100, 0, 0.0, 315.27000000000004, 177, 939, 569.100000000001, 804.9, 938.4799999999998, 1.8965975040776846, 1.8984681715377614, 0.7186326480294352], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=itinerary", 100, 0, 0.0, 307.0900000000001, 141, 463, 391.2000000000001, 436.24999999999983, 462.98, 2.0796073701285196, 3.552249647766502, 0.8001614295221062], "isController": false}, {"data": ["http:\/\/localhost:1080\/WebTours\/", 100, 0, 0.0, 10.249999999999998, 1, 142, 7.900000000000006, 138.39999999999964, 141.99, 1.9014298752662002, 1.2331998272075602, 0.7167499334499544], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=flights", 100, 0, 0.0, 331.97999999999996, 200, 783, 483.8, 534.0, 781.9299999999995, 2.01495093594471, 3.4417683902053238, 0.7713484051663342], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/reservations.pl?page=welcome", 100, 0, 0.0, 398.09000000000003, 218, 954, 569.2000000000003, 693.0999999999998, 953.2999999999996, 2.0236360692892994, 8.932732756091145, 0.7766493898737251], "isController": false}, {"data": ["Test", 100, 0, 0.0, 5520.059999999999, 4400, 9177, 8222.200000000012, 8976.8, 9176.18, 1.7414928076347045, 56.69647521855735, 15.384286238288462], "isController": true}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/login.pl?intro=true", 100, 0, 0.0, 364.04, 210, 832, 597.700000000001, 755.75, 831.5799999999998, 1.984126984126984, 2.2363668774801586, 0.7440476190476191], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=itinerary", 100, 0, 0.0, 285.83, 149, 444, 359.40000000000003, 375.69999999999993, 443.67999999999984, 2.0700076590283385, 1.6538229160197893, 0.8288116603531434], "isController": false}, {"data": ["http:\/\/localhost:1080\/WebTours\/header.html", 100, 0, 0.0, 2.11, 0, 25, 3.9000000000000057, 6.899999999999977, 24.949999999999974, 1.9067231056705947, 1.8769305571444916, 0.7019869246463029], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=search", 100, 0, 0.0, 328.12, 196, 963, 448.20000000000016, 703.9999999999991, 961.7499999999993, 2.0026434894060157, 1.6490126341771139, 0.7959725587775864], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/reservations.pl", 300, 0, 0.0, 364.89666666666653, 185, 751, 467.90000000000003, 491.0, 588.8800000000001, 6.01093991063736, 16.427515265282814, 4.281229337394057], "isController": false}, {"data": ["http:\/\/localhost:1080\/WebTours\/home.html", 200, 0, 0.0, 47.38999999999997, 2, 123, 59.0, 61.0, 64.99000000000001, 3.6777550982880047, 5.962668199830823, 1.3971159504238613], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?in=home", 200, 0, 0.0, 334.4850000000003, 120, 775, 432.0, 570.4499999999998, 757.97, 3.571109722346219, 6.050973406392286, 1.3705528524238906], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/login.pl", 100, 0, 0.0, 341.69999999999993, 158, 1175, 559.1000000000004, 735.95, 1174.2899999999997, 1.9644435713584125, 1.3275725002455554, 1.1356555716530792], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=home", 100, 0, 0.0, 355.56000000000006, 189, 901, 571.7, 709.1499999999994, 900.6999999999998, 1.9741777549650572, 3.3721809358589647, 0.7499561979310616], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/itinerary.pl", 100, 0, 0.0, 319.21000000000004, 131, 491, 389.5, 411.6499999999999, 490.46999999999974, 2.086419495503766, 2.3516025983746793, 0.768144677543867], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2000, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
